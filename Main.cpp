/*
 * Main.cpp
 *
 *  Created on: 9 gen 2019
 *      Author: Fabrizio Spoto
 */


#include <iostream>
#include "prova.h"
#include "prova2.h"

using namespace std;
//using namespace Prova;

int main(){
	using namespace Prova;
	Prova::prova *y= new Prova::prova();
	Prova::prova *z= new Prova::prova();

	y->incr();
	y->incr();

	z->incr();

	cout<<"y: "<< y->get()<< endl;
	cout<<"z: "<< z->get()<< endl;

	Prova::prova *h= new Prova::prova();
	cout<<"h: "<< h->get()<< endl;

	static const int i=9;


	cout<<"i: " << i << endl;

	prova2::prova2 *l= new prova2::prova2();

	cout<<"l: " << l->get_i() << endl;


	return 0;
}
