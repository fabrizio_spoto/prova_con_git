/*
 * prova.h
 *
 *  Created on: 9 gen 2019
 *      Author: Fabrizio Spoto
 */

#ifndef PROVA_H_
#define PROVA_H_
namespace Prova{
class prova {
public:
	prova();
	virtual ~prova();
	void incr();
	int get();

//private:
	static int x;
};
}

#endif /* PROVA_H_ */
