/*
 * prova2.h
 *
 *  Created on: 9 gen 2019
 *      Author: Fabrizio Spoto
 */

#ifndef PROVA2_H_
#define PROVA2_H_

namespace prova2 {

class prova2 {
public:
	prova2();
	virtual ~prova2();
	int get_i();
private:
	static int i;
};

} /* namespace prova2 */

#endif /* PROVA2_H_ */
